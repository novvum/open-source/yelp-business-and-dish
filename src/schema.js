import {
  makeRemoteExecutableSchema,
  introspectSchema,
  mergeSchemas
} from "graphql-tools";
import { HttpLink } from "apollo-link-http";
import fetch from "node-fetch";

const link = new HttpLink({
  uri: "https://api.yelp.com/v3/graphql",
  fetch,
  headers: {
    Authorization: "Bearer r5b9jmTLJGj4_qdDDZtWISM_6QoX4uDHYoEb2JneFCfD4aUDPm5__J4cN6uDxUE-xccigJw_WylE86bkBAFoSqIjQ8YcZ8pF6TDGHyQtZn7WdnCRrRpSr2O44aBHW3Yx"
  }
});

const getYelpSchema = async () => {
  const schema = await introspectSchema(link);

  const executableSchema = makeRemoteExecutableSchema({
    schema,
    link
  });

  return executableSchema;
};

//ERROR: SyntaxError: Unexpected token I in JSON at position 0
//Ignore these for now, fix later.
const dishLink = new HttpLink({
  uri: "https://api.graph.cool/simple/v1/cjjeqxhxj4klh01468zmazh9w",
  fetch
});
const getDishSchema = async () => {
  const schema = await introspectSchema(dishLink);
  const executableSchema = makeRemoteExecutableSchema({
    schema,
    link: dishLink
  });
  return executableSchema;
};

const linkTypeDefs = `
	extend type Dish{
		yelpBusiness: Business
	}	
	extend type Business{
		dish: Dish
  }
  extend type Mutation {
    linkBusinessToDish(business: String!, name: String, ranking: Int!, ratingTotal: Int!, tags: [String!]!, ratingsIds: [ID!], ratings: [DishratingsRating!]): Dish
  }
`;

async function makeSchema() {
  const dishSchema = await getDishSchema();
  const yelpSchema = await getYelpSchema();
  return mergeSchemas({
    schemas: [dishSchema, linkTypeDefs, yelpSchema],
    resolvers: {
      Mutation: {
        async linkBusinessToDish(parent, args, context, info) {
          const createdDish = await info.mergeInfo.delegateToSchema({
            operation: "mutation",
            fieldName: "createDish",
            info,
            args,
            context,
            schema: dishSchema
          });
          return createdDish;
        }
      },
      Dish: {
        yelpBusiness: {
          fragment: "fragment DishFragment on Dish { id }",
          resolve(parent, args, context, info) {
            return info.mergeInfo.delegate(
              "query",
              "dishByBusinessId",
              {
                businessId: parent.id
              },
              context,
              info
            );
          }
        }
      },
      Business: {
        dish: {
          fragment: "fragment businessFragment on Business { id }",
          resolve(parent, args, context, info) {
            return info.mergeInfo.delegate(
              "query",
              "Dish",
              {
                id: parent.dishId
              },
              context,
              info
            );
          }
        }
      }
    }
  });
}

export const schema = makeSchema();
